# ProCoders React Native Learning course

Изначально необходимо знать и разбираться в [React](./react.md)

## Базовые видео курсы

### Новые обучалки:

* Сокращенный курс на русском [React Native 2020. Мобильные приложения на JavaScript](https://coursehunter.net/course/react-native-mobilnaya-razrabotka-na-javascript-i-react-js)
* Полный курс на English [Video React Native - The Practical Guide](https://coursehunter.net/course/react-native-prakticheskoe-rukovodstvo)


### Общиие ссылки:
* Новейшая видео обучалка React Native + GraphQL[Building AirBnb Clone](https://www.youtube.com/playlist?list=PLN3n1USn4xlnfJIQBa6bBjjiECnk6zL6s). Крайне рекомендовано к просмотру как для изучения React Native так и для React Apollo! 
* [Official tutorial](https://facebook.github.io/react-native/docs/tutorial.html)
* Собрание обучалок [Tutorials React Native](https://proglib.io/p/react-native-tutorials/)
* [Полный курс React Native, создайте красивые приложения ](https://coursehunter.net/course/polnyy-kurs-react-native-sozdayte-krasivye-prilozheniya). Доктор рекомедует (детальное описание установки зависимостей и библиотек).
* [Информативный воркшоп по React Native, AWS и GraphQL](https://github.com/dabit3/aws-amplify-workshop-react-native). Использует АКТУАЛЬНЫЙ AWS-amplify вместо DEPRECATED awsmobile (с использованием которого создано большинство доступных туториалов)!


## Тестовые задания

* [Тестовое задание №1](tasks/react_native/task1.md) - простое приложение для управления задачами

### 1. Общие статьи 
* Быстрое введение-обзор в [React Native одного JS мало](https://habr.com/post/323214/)
* [React Native с точки зрения мобильного разработчика](https://habr.com/company/qlean/blog/416097/) позволяет более глубже понять мир мобильных приложений
* изучить список плагинов для работы с периферией [Awesome React Native](https://github.com/jondot/awesome-react-native#system)
* ознакомиться с UI компонентами, чтобы не писать велосипед [Awesome React Native UI](https://github.com/madhavanmalolan/awesome-reactnative-ui)
* ументь поддержать holly-war [Ionic 2 vs React Native: сравнение фреймворков](https://habr.com/post/328960/)

### 2. Boilerplates & StarterKits

Всегда помним, что не нужно собирать каждый раз с нуля все руками. За вас это уже сделали!!!

### 2.1 Ignite CLI

Самый модный бойлер с консольным генератором [ignite CLI](https://infinite.red/ignite)
* классика (ignite + redux) https://github.com/infinitered/ignite-ir-boilerplate-andross
* или ignite + native base https://github.com/GeekyAnts/ignite-native-base-boilerplate
* или добавим https://react-native-training.github.io/react-native-elements

добираем [плагины ignite](https://github.com/infinitered/ignite/blob/master/PLUGINS.md) : firebase, img-cache, redux-devtools, redux-persist 

### 2.2 Boilerplates
* a React Native + React (web) & Firebase (optional) on single codebase [boilerplate](https://github.com/mcnamee/react-native-starter-kit)
* неплохой стартер ReactNative + TS + CodePush [RN TS](https://ueno-llc.github.io/react-native-starter/#/PUBLISHING)
* [seed starter](https://reactnativeseed.com/)

### 2.3 Fast design

* [BuilderX](https://builderx.io/)

### 3. Плагины и компоненты
* [React Native Navigation](https://github.com/wix/react-native-navigation)
* [ReactNative Firebase](https://rnfirebase.io/) как важная часть инфраструктуры приложения. Разбираться с: Cloud Messaging, Crashlytics, Dynamic Links, 
* важные плагины react-native-firebase, react-native-permissions, react-native-notifications, react-native-background-fetch, react-native-touch-id, react-native-in-app-utils, react-native-barcodescanner react-native-location, react-native-social-share, react-native-motion-manage, react-native-android-sms-listener, react-native-mauron85-background-geolocation, react-native-config
* [React Native Config](https://github.com/luggit/react-native-config#different-environments) Наиболее стабильный конфиг для переменных окружения. Не работает с Expo!

#### 3.1 Построение интерфейса
* [NativeBase](https://nativebase.io/) как основные компоненты для приложения
* [Анимации](https://coursehunters.net/course/rukovodstvo-po-animaciyam-v-react-native)
* [Прелести CSS в рамках React Native](https://github.com/vitalets/react-native-extended-stylesheet) React Native Extended StyleSheet

### 4. Специфика и нативность
* Пишем собственный нативный модуль [...все возможности мобильной ОС в React Native](https://habr.com/company/epam_systems/blog/347346/)
* Почему я не могу вызвать http c Android?[И как с этим бороться? Также отвечает на вопрос: почему наглухо виснет Android версия, если в побилденном APK остались вызовы console](http://www.douevencode.com/articles/2018-07/cleartext-communication-not-permitted/)

### 6. Deploy, сборка и заливка

Понимание процесса

* [Понимание сборки Android SDK и Gradle](https://medium.com/androiddevelopers/picking-your-compilesdkversion-minsdkversion-targetsdkversion-a098a0341ebd)
* Решение [Gradle конфиликтов](https://github.com/transistorsoft/react-native-background-fetch/wiki/Solving-Android-Gradle-Conflicts) при сборке 
* [CodePush](https://github.com/Microsoft/react-native-code-push) для обновления приложений без документации
* [Смена названия приложения](https://medium.com/@impaachu/how-to-change-bundle-identifier-of-ios-app-and-package-name-of-android-app-within-react-native-app-4fbdd6679aa2)
* [Добавление шрифтов](https://medium.com/@kswanie21/custom-fonts-in-react-native-tutorial-for-ios-android-76ceeaa0eb78)Только чистый RN, не Expo. (Настоятельно рекомендую обратить внимание на выведение в консоль xCode названий шрифтов, они отличаются от названия файлов!)
* [adHoc build или как правильно накатить?](https://codeburst.io/latest-itunes-12-7-removed-the-apps-option-how-to-install-ipa-on-the-device-3c7d4a2bc788) Устанавливаем готовое приложение на iPhone в обход AppStoreConnect.
### 7. Push-Notifications

"Чистый" React Native (не Expo/Create-React-Native-App)

* [На обеих платформах при помощи Firebase](https://medium.com/@anum.amin/react-native-integrating-push-notifications-using-fcm-349fff071591)
* [Раздельно при помощи react-native-push-notification library](https://apiko.com/blog/react-native-push-notifications/)

Expo/Create-React-Native-App

* [Все по официальной документации](https://docs.expo.io/versions/latest/guides/push-notifications/)

### 8. Утилиты


* [Создание кастомных шрифтов из SVG иконок](https://www.reactnative.guide/12-svg-icons-using-react-native-vector-icons/12.1-creating-custom-iconset.html)
* [app-icon](https://www.npmjs.com/package/app-icon)

### 9. Баги
Ссылки на фиксы багов, которые, чаще всего, лечатся одной строкой кода, могут намертво заморозить ваше приложение

* [React Native Config - переменные окружения работают при старте из консоли, но билд валится при старте из xCode](https://github.com/luggit/react-native-config/issues/174)

### 10. Обновление React-Native

* [React Native Upgrade Helper - показывает, что именно нужно поменять в нативных файлах в виде git diff](https://react-native-community.github.io/upgrade-helper)
