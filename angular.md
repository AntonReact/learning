# ProCoders Angular Learning course

## Базовые видео курсы

[Дерево обучения](https://edu.cbsystematics.com/Images/RoadMap/Roadmap_Frontend-min.jpg)

[Мир Angular](https://medium.com/@splincode/%D1%83%D0%B4%D0%B8%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9-angular-42238622d170)

### 1. Javascript
* [Learn JS for dummies](http://www.learn-js.org/)
* разобраться в деталях ES2015 на основе [презентации](http://courseware.codeschool.com.s3.amazonaws.com/es2015-the-shape-of-javascript-to-come/all-levels.pdf) знать досконально!

### 2. Пишем на Typescript

* разобраться с [TypeScript за 5 минут](https://habr.com/ru/company/ruvds/blog/344502/)
* [Typescript OOP](https://www.typescriptlang.org/docs/handbook/basic-types.html)
* [TypeScript: 20 вопросов и ответов](https://habr.com/ru/company/ruvds/blog/419993/)
* [Рефлексия в Typescript](https://blog.wizardsoftheweb.pro/typescript-decorators-reflection/)



#### 2.1. Development principles

Отдельная документация [о принципах разработки ПО](principles.md)

Специфика для angluar:

* [SOLID in Typescript](https://medium.com/@ashu.singh212/s-o-l-i-d-in-typescript-c0e4fe6c345a)
* [S.O.L.I.D principles with Angular](https://youtu.be/KIJdNhciTvU)
* [5 вещей, которые я бы хотел знать, когда начинал использовать Angular](https://habr.com/ru/company/tinkoff/blog/480782/)

### 3. Видео-курсы Angular

Самый простой и понятный [курс Angular](http://learn.javascript.ru/screencast/angular) на русском

Слушать видно курсы можно на скорости х1.25 и даже разогнаться до х1.75. Используйте [VLC player](https://www.videolan.org/index.ru.html)
* Самый полный курс 28 часов на английском языке от Udemy [Angular 6. The complete guide](https://rutracker.org/forum/viewtopic.php?t=5534170)
* Более краткий мастер-класс от Udemy на 15+ часов [TypeScript, Angular, Firebase & Angular Material Masterclass](https://rutracker.org/forum/viewtopic.php?t=5548685)

Если же с английским язком совсем "тяжело", тогда качаем и смотрим для убогих
* [Angular 4 с Нуля до Профи](https://rutracker.org/forum/viewtopic.php?t=5471796)
* [Продвинутый курс Angular 6](https://coursehunters.net/course/prodvinutyy-kurs-po-angular) от learn.javascript.ru


### 4. Angular base patterns
* [Styleguide от лысого из браззерс](https://angular.io/guide/styleguide)
* [Best pracs для новичков](https://codeburst.io/angular-best-practices-4bed7ae1d0b7)
* [Effective Component Patterns](https://itnext.io/angular-effective-component-patterns-f5f7f08e2072)
* Книга на английском [Angular Design Patterns](https://yadi.sk/d/p1ytQtXTbJTIyw)

* Подход [Angular Elements](https://medium.com/@kate.gunko.work/%D0%BE%D0%B1%D0%B7%D0%BE%D1%80-angular-elements-%D0%BD%D0%BE%D0%B2%D0%BE%D0%B5-%D0%B1%D1%83%D0%B4%D1%83%D1%89%D0%B5%D0%B5-front-end-%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B8-%D1%83%D0%B6%D0%B5-%D0%BE%D1%87%D0%B5%D0%BD%D1%8C-%D0%B1%D0%BB%D0%B8%D0%B7%D0%BA%D0%BE-57dfd22b260b)

#### 4.1 RxJS
RxJS - ключевая библиотека Angular

* [Что такое RxJS](https://habr.com/company/ruvds/blog/341880/)
* [RxJS за 1 час](https://www.youtube.com/watch?v=gCwSVQO_PtY) на русском
* Видео [раздупляемся с RxJS за 50 минут](https://www.youtube.com/watch?v=3rEDHnqn-Cw) и [Основное в RxJS](https://aalexeev239.github.io/rxjs-intro/) в виде слайдов
* RxJS: [все о Subjects, Behavior Subjects и Replay Subjects](https://golosay.net/rxjs-subjects/)

**Важно**: пройти [RxJS tutorial](https://webdraftt.com/tutorial/rxjs)

* Не забывать отписывать компоненты [6 Ways to Unsubscribe from Observables](https://blog.bitsrc.io/6-ways-to-unsubscribe-from-observables-in-angular-ab912819a78f) or [Unsubscribe For Pros](https://www.npmjs.com/package/@ngneat/until-destroy) package


#### 4.2 FLUX pattern

Начинать с [FLUX в картинках](https://habr.com/ru/company/hexlet/blog/268249/), а также Общее [понимание FLUX](https://habr.com/ru/post/249279/)

NGRX as most popular Angular Store pattern 

* Статья в трех частях [Реактивные приложения на Angular](https://medium.com/@demyanyuk/%D1%80%D0%B5%D0%B0%D0%BA%D1%82%D0%B8%D0%B2%D0%BD%D1%8B%D0%B5-%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F-%D0%BD%D0%B0-angular-ngrx-%D1%87%D0%B0%D1%81%D1%82%D1%8C-1-cb7b4f2852dc)
* [angular-ngrx-data — state management и CRUD за пять минут](https://habr.com/ru/post/418369/)


**Важно**: пройти [NgRx tutorial](https://webdraftt.com/tutorial/ngrx)

MobX as alternative
* Альтернативный FLUX [MobX with Angular: the Prelude](https://medium.com/@vivainio/mobx-with-angular-the-prelude-1c0dcfb43fe6)

#### 4.3 Best practices
* [Automagically Unsubscribe in Angular](https://netbasal.com/automagically-unsubscribe-in-angular-4487e9853a88) + [angular2-take-until-destroy](https://github.com/NetanelBasal/angular2-take-until-destroy)
* [Angular Schematics](https://medium.com/@tomastrajan/%EF%B8%8F-how-to-create-your-first-custom-angular-schematics-with-ease-%EF%B8%8F-bca859f3055d) и [Simple Schematic](https://medium.com/rocket-fuel/angular-schematics-simple-schematic-76be2aa72850)
* 44 quick tips to fine-tune [Angular performance](https://medium.com/@spp020/44-quick-tips-to-fine-tune-angular-performance-9f5768f5d945)

#### 4.4 Changes detection & NgZone

Желательно изучить в порядке следования

* [Все, что вам нужно знать об обнаружении изменений в Angular](https://habr.com/ru/post/327004/)
* [Полное руководство по стратегии обнаружения изменений Angular onPush](https://habr.com/ru/company/infopulse/blog/358860/)
* [ngZone for better performance](https://blog.thoughtram.io/angular/2017/02/21/using-zones-in-angular-for-better-performance.html)
* [NgZone/Zone.js in details](https://blog.bitsrc.io/how-angular-uses-ngzone-zone-js-for-dirty-checking-faa12f98cd49)

#### 4.5 Декораторы
* [Кешируем Observable](https://dev.to/davidecavaliere/rxjs-caching-observables-with-a-decorator-12fe) с помощью простенького декторатора 
* Декотора класса на примере [Analytics Tracking pages](https://medium.com/prototype-berlin/analytics-in-ionic-3-easily-track-page-views-with-a-custom-decorator-33aa96e5a499)


### 5. Базовые пакеты для Angular

* Работа с датами momment.js [ngx-moment](https://github.com/urish/ngx-moment)

* SEO через [Meta tags](https://www.npmjs.com/package/@ngx-meta/core)
* Аналитика через [Segment](https://github.com/opendecide/ngx-segment-analytics)


### 6. Тестироваие в Angular
* Статья о тестировании [AngularTesting Guide v4+](https://medium.com/google-developer-experts/angular-2-testing-guide-a485b6cb1ef0)
* Видеокурс [Play by Play: Fundamentals of Angular Testing](https://rutracker.org/forum/viewtopic.php?t=5588880)

#### 6.1. E2E Tests

Быстрые Подсказки [Finally a decent ProtractorJS cheatsheet](https://blog.makersacademy.com/finally-a-decent-protractorjs-cheatsheet-cc7a074d74a0) и 


### 7. Коммуникации с сервисами
* Работа с Firebase в Angular [Building a Realtime Chat Application with Angular and Firebase](https://rutracker.org/forum/viewtopic.php?t=5548675)

### 8. Ionic

#### 8.1 Generic Ionic 

* Видео курс Ionic3 + backend [Криптовалютное приложение с backend-ом](https://coursehunters.net/course/ionic-3-sozdaem-krasivoe-kriptovalyutnoe-prilozhenie)
* Лепим простенький калькулятор по статье [Getting started with Ionic](https://medium.com/craft-academy/getting-started-with-ionic-5a05b1ba8293)
* Делаем более сложное приложение по статье [Создание приложения на Ionic с использованием API](https://habr.com/post/344474/)

#### 8.2 Ionic markup

Ionic 4 documentation themes and markup:
* [Angular Template syntax](https://angular.io/guide/template-syntax)
* [Theming advanced](https://ionicframework.com/docs/theming/advanced)
* [Layout css](https://ionicframework.com/docs/layout/css-utilities)
* [Routing and navigation lesson](https://angularfirebase.com/lessons/ionic-4-routing-and-navigation-guide/)
* [Using Modals](https://medium.com/@david.dalbusco/how-to-declare-and-use-modals-in-ionic-v4-4d3f42ac30a3)

#### 8.3. Ionic Auth

* [Ionic Facebook Auth](https://ionicthemes.com/tutorials/about/ionic-facebook-login)
* [Ionic Google Auth](https://ionicthemes.com/tutorials/about/ionic-google-login)

* [Angular PWA + AWS Amplify](https://itnext.io/part-1-building-a-progressive-web-application-pwa-with-angular-material-and-aws-amplify-5c741c957259)

#### 8.4. Ionic interface

* [mobiscroll components](https://demo.mobiscroll.com/angular)


#### 8.5. Ionic plugins

* [How to write Cordova Plugins](https://medium.com/ionic-and-the-mobile-web/how-to-write-cordova-plugins-864e40025f2)
* [Go From 0 to Your OutSystems Mobile App](https://www.outsystems.com/blog/posts/how-to-create-a-cordova-plugin-from-scratch/)
* [How to Create a Cordova Plugin from Scratch](https://medium.com/@sharman94kuldeep/simple-recipe-for-custom-cordova-plugin-e91578f146d0)

### 9. Утилиты и библиотеки
* [Angular Schematics](https://medium.com/@jorgeucano/your-fist-angular-schematics-f711d70cb37c) для написания собственных шаблонов cli
* [Json-Server](https://medium.com/codingthesmartway-com-blog/create-a-rest-api-with-json-server-36da8680136d) для быстрого мока бекенда 
* [Мышление в стиле Ramda: Первые шаги](https://habr.com/post/348868/)

### 10. GraphQL
* [Что такое GraphQL](https://habr.com/post/326986/)
* [Переход от REST API к GraphQL](https://habr.com/post/334182/)
* [Complete video Tutorial for GraphQL](https://www.howtographql.com/)

Смотреть инфо по Apollo и связи с Angular

### 11. Server Rendering Angular

* Angular Universal
* [Создание микро-фронтендов с использованием Angular Elements](https://habr.com/ru/post/485326/)



## Общие скиллы

### Подготовка к собесу
* [Вопросы для джуна](https://www.youtube.com/watch?v=Lyf0MyA1stM) для вопросов к junior
* [Видео с 50 вопросами](https://www.youtube.com/watch?v=jLY5Hg9k-PE) для подготовки к собесам
* [RxJS interview](https://www.youtube.com/watch?v=nrEbLLmVBrc) для собесов

### 1. VsCode
* [Tips and Tricks for VSCode](https://github.com/Microsoft/vscode-tips-and-tricks)

### 2. Принципы программирования
* [Паттерны](https://www.youtube.com/watch?v=Z90DFL2Ndow)
* [SOLID принципы](https://www.youtube.com/watch?v=59tq5Fcgn7A)
